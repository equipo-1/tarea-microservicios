##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: telegram_controller.py
# Capitulo: Estilo Microservicios
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Version: 3.0.0 Febrero 2022
# Descripción:
#
#   Ésta clase define el controlador del microservicio API. 
#   Implementa la funcionalidad y lógica de negocio del Microservicio.
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |     send_message()     |         Ninguno          |  - Procesa el mensaje |
#           |                        |                          |    recibido en la     |
#           |                        |                          |    petición y ejecuta |
#           |                        |                          |    el envío a         |
#           |                        |                          |    Telegram.          |
#           +------------------------+--------------------------+-----------------------+
#
#-------------------------------------------------------------------------
from flask import request, jsonify
import json, os, telepot, wget
from src.helpers import config

class TelegramController:

    @staticmethod
    def send_message():
        data = json.loads(request.data)
        # print(data)
        if not data:
            return jsonify({"msg": "invalid request"}), 400


        # Lee el archivo con las variables de entorno.
        cfg = config.load_config()

        # Se extra el mensaje proviniente del request y se definen el token y id de chat para el telebot.
        mensaje = data['message']
        url_poliza = data['url']
        token_telegram = cfg['TELEGRAM']['TOKEN']
        id_chat = cfg['TELEGRAM']['CHAT_ID']

        # Se descarga el archivo generado por el URL.
        wget.download(url_poliza, 'Poliza.pdf')
        archivo = open('Poliza.pdf', 'rb')
        
        # Se instancia el telebot usando el token y se envia el mensaje al id extraido de la variable de entorno.
        bot = telepot.Bot(token_telegram)
        bot.sendMessage(id_chat, mensaje)
        bot.sendDocument(id_chat, archivo)

        # Se elimina la poliza descarga con wget.
        os.remove('Poliza.pdf')

        # Regresa un codigo satisfactorio 
        return jsonify({"msg": "mensaje enviado"}), 200