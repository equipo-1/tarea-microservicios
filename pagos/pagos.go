package main

import (
	// "encoding/json"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", Index)
	router.HandleFunc("/records", GetPayments)
	log.Fatal(http.ListenAndServe(":8003", router))
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}

func GetPayments(w http.ResponseWriter, r *http.Request) {
	// Se realiza la lectura de un archivo, en esto caso el generado por el simulador de pagos.
	datosPagos, err := ioutil.ReadFile("/data/payment_records.json")

	// Verificar que no se haya arrojado un error.
	if err != nil {
		log.Fatal(err)
	}

	
	// Se manda la cabecera con el codigo satisfactorio.
	w.WriteHeader(200)
	// Y se escribe en el cuerpo(body) para que la parte del cliente la lea y decodifique.
	// w.Write(datosPagos)

	// fmt.Fprintf(w, "%q Aun o implementado", html.EscapeString(r.URL.Path))
	// Envia la informacion cargada de los registros de pagos, directamente al HTML
	fmt.Fprintf(w, string(datosPagos))
}
